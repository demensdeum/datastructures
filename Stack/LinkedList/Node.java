package com.demensdeum.datastructures;

public class Node<T> {

    public Node<T> next;
    public T item;
    
}
