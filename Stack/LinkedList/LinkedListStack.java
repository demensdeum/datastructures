package com.demensdeum.datastructures;

import com.demensdeum.datastructures.*;

public class LinkedListStack<T> implements Stack<T> {

    private Node<T> first;

    @Override
    public void push(T item) {
        var next = first;
        first = new Node();
        first.item = item;
        first.next = next;
    }
    
    public T pop() {
        var output = first.item;
        var first = this.first.next;
        this.first = first;
        return output;
    }
    
    public boolean isEmpty() {
        return first == null;
    }

}
