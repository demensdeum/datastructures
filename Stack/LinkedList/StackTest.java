package com.demensdeum;

import java.util.concurrent.TimeUnit;
import com.demensdeum.datastructures.*;

public class StackTest
{ 
    public static void main(String args[]) 
    { 
        var stack = new LinkedListStack<String>();
        stack.push("Defence");
        stack.push("My");
        stack.push("OOO!");
        while (stack.isEmpty() == false) {
            System.out.println(stack.pop());
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            }
            catch(Exception exception) {
            }
        }
        for (int i = 0; i < 2000; i++) {
            stack.push(String.valueOf(i));
        }
    } 
} 
