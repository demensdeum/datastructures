var DEBUG = 0;
var BROKEN_HASH_FUNCTION = 0;

function debugPrint(string) {
    if (DEBUG == 1) {
        console.log(string);
    }
}

class KeyValuePair {
    key = null;
    value = null;
    
    constructor(key, value) {
        this.key = key;
        this.value = value;
    }
}

class Node {
    value = null;
    next = null;
    
    constructor(value) {
        this.value = value;
        this.previous = null;
        this.next = null;
    }
}

class HashMap {

    _array = new Array();
    _arraySize = 65535;
    
    constructor() {
        for (var i = 0; i < this.arraySize; i++) {
            this._array[i] = null;
        }
    }
    
    _getKeyValuePair(key) {
        debugPrint("_getKeyValuePair(" + key + ")");
        var hash = this._hash(key);
        var node = this._array[hash];
        while (node != null) {
            debugPrint("get not null: " + node.value.key);
            if (node.value.key == key) {
                debugPrint("node.value.key: " + node.value.key + " == " + key);
                return node.value
            }
            else {
                debugPrint("node.value.key: " + node.value.key + " != " + key);
            }
            node = node.next;
        }
        
        debugPrint("return key - null for key: " + key);
        return null;
    }
    
    get(key) {
        var keyValuePair = this._getKeyValuePair(key);
        if (keyValuePair) {
            return keyValuePair.value;
        }
        return null;
    }
    
    set(key, value) {
        
        debugPrint(key + " = " + value);
        
        var hash = this._hash(key);
        var node = this._array[hash];
        
        if (node == null) {
            this._array[hash] = new Node(new KeyValuePair(key, value));
            debugPrint("exit 1");
            return;
        }
        
        while (true) {
            if (node.value.key == key) {
                node.value.value = value;
                debugPrint("exit 2");
                return;
            }
            else if (node.next == null) {
                node.next = new Node(new KeyValuePair(key, value));
                node.previous = node;
                debugPrint("exit 3");
                return;
            }
            else {
                node = node.next;
                debugPrint("exit 4");
            }
        }
    }
    
    remove(key) {
        var keyValuePair = this._getKeyValuePair(key);
        if (keyValuePair) {
            if (keyValuePair.previous != null) {
                keyValuePair.previous.next = keyValuePair.next;
            }
            else {
                var hash = this._hash(key);
                var rootNode = this._array[hash];
                this._array[hash] = rootNode.next;
            }
        }
    }
    
    _hash(key) {
        var hash = 0;
        if (BROKEN_HASH_FUNCTION) {
            hash = 1;
        }
        else {
            for (let character in key) {
                hash += character.charCodeAt(0);
            }
        }
        return parseInt(hash) % this._arraySize;
    }
    
};

var hashmap = new HashMap();
hashmap.set("key", "value");
hashmap.set("key", "val");
hashmap.set("keyy2", "valueee2");
console.log("TEST1 (val): " + hashmap.get("key"));
hashmap.remove("key");
console.log("TEST2 (null): " + hashmap.get("key"));
console.log("TEST3 (valueee2): " + hashmap.get("keyy2"));
