package com.demensdeum.datastructures;

public interface Stack<T> {
    public void push(T item);
    public T pop();
    public boolean isEmpty();
}
