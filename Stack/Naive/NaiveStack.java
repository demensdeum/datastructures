package com.demensdeum.datastructures;

import com.demensdeum.datastructures.*;

public class NaiveStack<T> implements Stack<T> {

    private int currentIndex = -1;
    private Object[] innerCollection = new Object[1000];

    @Override
    public void push(T item) {
        currentIndex++;
        if (currentIndex >= innerCollection.length) {
            throw new RuntimeException("Naive Stack Overflow");
        }
        innerCollection[currentIndex] = item;
    }
    
    public T pop() {
        if (currentIndex < 0) {
            return null;
        }
        @SuppressWarnings("unchecked")
        var output = (T) innerCollection[currentIndex];
        currentIndex--;
        return output;
    }
    
    public boolean isEmpty() {
        return currentIndex < 0;
    }

}
